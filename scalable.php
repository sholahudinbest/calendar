<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        #canvas {
            width:297px;
            height:420px;
            border:1px solid #111;
        }
    </style>
</head>
<body>
    <div id="canvas">
        <h1 data-font-size="150">2018</h1>
        <p data-font-size="40">Lorem Ipsum</p>
    </div>

<script src="js/jquery-3.2.1.min.js"></script>

<script>
    $(document).ready(function(){
        var actualWidth = 297;
        var actualHeight = 420;

        var targetWidth = 297;
        var targetHeight  = actualHeight / actualWidth * targetWidth;

        $("#canvas").width(targetWidth);
        $("#canvas").height(targetHeight);

        $("[data-font-size]").each(function(i, val){
            var actualSize = $(val).attr("data-font-size");
            var newSize = targetWidth / actualWidth * actualSize;

            $(val).css("font-size", newSize+'px');
        });
    });
</script>
</body>
</html>