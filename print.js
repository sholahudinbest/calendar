var page = new WebPage();
var system = require("system");
// change the paper size to letter, add some borders
// add a footer callback showing page numbers
page.viewportSize = {
    width:420,
    height:297
}
page.settings.userAgent = "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Safari/537.36";
page.paperSize = {
    format: "a3",
    orientation: "landscape",
    margin: { left: "0cm", right: "0cm", top: "0cm", bottom: "0cm" },
    // footer: {
    //     height: "0.9cm",
    //     contents: phantom.callback(function (pageNum, numPages) {
    //         return "<div style='text-align:center;'><small>" + pageNum +
    //             " / " + numPages + "</small></div>";
    //     })
    // }
};
page.zoomFactor = 0;
// assume the file is local, so we don't handle status errors
page.open(system.args[1], function (status) {
    // export to target (can be PNG, JPG or PDF!)
    page.render(system.args[2]);
    phantom.exit();
});