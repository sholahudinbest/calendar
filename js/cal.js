$(document).ready(function () {
    var actualWidth = 420;
    var actualHeight = 297;
    var targetWidth = 1347;

    var newHeight = actualHeight / actualWidth * targetWidth;
    console.log(newHeight);

    $(".paper").css({
        "height":newHeight + "px",
        "width":targetWidth+ "px"
    });


    var calc = function(size, unit){
        var newSize =  targetWidth / actualWidth * size;
        return newSize+unit;
    };


    var previewAs = function(targetWidth, unit){

    }

    $("[data-font-size]").each(function (i, val) {
        var actualSize = $(val).attr('data-font-size');
        var newSize = calc(actualSize, 'px');
        $(val).css('font-size', newSize);
    })
    $("[data-width]").each(function(i, val){
        var actualSize = $(val).attr("data-width");
        var newSize = calc(actualSize, 'px');
        $(val).css('width', newSize);
    });
    $("[data-height]").each(function(i, val){
        var actualSize = $(val).attr("data-height");
        var newSize = calc(actualSize, 'px');
        $(val).css('height', newSize);
    });
    $("[data-margin-top]").each(function(i, val){
        var actualSize = $(val).attr("data-margin-top");
        var newSize = calc(actualSize, 'px');
        $(val).css('margin-top', newSize);
    });
    $("[data-margin-left]").each(function(i, val){
        var actualSize = $(val).attr("data-margin-left");
        var newSize = calc(actualSize, 'px');
        $(val).css('margin-left', newSize);
    });
})