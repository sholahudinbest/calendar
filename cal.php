<?php
$current_month=date('n');
$current_year=date('Y', strtotime('+1 year'));
$current_day=date('d');
$months=array('JANVIER '.$current_year, 'FÉVRIER '.$current_year, 'MARS '.$current_year, 'AVRIL 
'.$current_year, 'MAI '.$current_year, 'JUIN '.$current_year, 'JUILLET '.$current_year, 'AOÛT '.$current_year, 'SEPTEMBRE '.$current_year, 'OCTOBRE '.$current_year, 'NOVEMBRE '.$current_year, 'DÉCEMBRE '.$current_year);
$month=0;
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Calendar</title>
    <link rel="stylesheet" href="scss/cal.css">
</head>
<body>
    <div class="paper">
        <div class="wrap-img" data-width="80"></div>
        <div class="calendar" data-height="297" data-width="320">
            <div class="wrap-title" data-width="200" data-margin-top="15">
                <div class="year" data-height="30">
                    <span data-font-size="30">2018</span>
                </div>
                <div class="title" data-height="30">
                    <span data-font-size="7">Le club de mon coeur</span>
                </div>
            </div>
            <div class="parent-col" data-margin-top="10" data-margin-left="15">
            <?php
            for($row=1; $row<=3; $row++) {
                for($column=1; $column<=4; $column++) {

                    $month++;

                    $first_day_in_month=date('w',mktime(0,0,0,$month,1,$current_year));
                    $month_days=date('t',mktime(0,0,0,$month,1,$current_year));


                    if ($first_day_in_month==0){
                        $first_day_in_month=7;
                    }
            ?>
                        <div class="col" data-width="60" data-height="65"  data-margin-top="5" data-margin-left="1">
                            <div class="month" data-font-size="6" data-height="10" data-width="60"><?php echo $months[$month-1]?></div>
                            <div class="group-dates" data-height="55" data-width="60">
                                <span>
                                    <div class="days">
                                        <span>Lun</span><span>Mar</span><span>Mer</span><span>Jeu</span><span>Ven</span>
                                        <span class="sat">Sam</span><span class="sun">Dim</span>
                                    </div>
                                <span>
                                <?php
                                for($i=1;$i<=6; $i++){
                                ?>
                                    <div class="col-dates" data-width="59" data-height="8">
<!--                                            <div class="col-dates" data-width="8" data-height="64">-->
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
            <?php
                }
            }
            ?>
            </div>
        </div>
    </div>
</body>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="js/cal.js"></script>
</html>