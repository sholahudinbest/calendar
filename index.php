<?php
$current_month=date('n');
$current_year=date('Y', strtotime('+1 year'));
$current_day=date('d');
$months=array('JANVIER '.$current_year, 'FÉVRIER '.$current_year, 'MARS '.$current_year, 'AVRIL 
'.$current_year, 'MAI '.$current_year, 'JUIN '.$current_year, 'JUILLET '.$current_year, 'AOÛT '.$current_year, 'SEPTEMBRE '.$current_year, 'OCTOBRE '.$current_year, 'NOVEMBRE '.$current_year, 'DÉCEMBRE '.$current_year);
$month=0;

echo "<head>";
    echo '<meta charset="utf-8">';
    echo "<link rel=\"stylesheet\" href=\"scss/style.css\">";
echo "</head>";
echo "<div class='paper a3'>";
    echo '<div class="wrap-img"></div>';
    echo '<table class="calendar">';
//    echo '<th></th>';
    echo '<th colspan="4">
            <div class="wrap-title">
                <div class="year" data-font-size="30">'.$current_year.'</div>
                <div class="title" data-font-size="12">365 JOURS <br> DE GOURMANDISE </div>
            </div>
          </th>';
//    echo '<th></th>';

    for ($row=1; $row<=3; $row++) {
        echo '<tr>';
        for ($column=1; $column<=4; $column++) {
            echo '<td class="month">';

            $month++;

            $first_day_in_month=date('w',mktime(0,0,0,$month,1,$current_year));
            $month_days=date('t',mktime(0,0,0,$month,1,$current_year));


            if ($first_day_in_month==0){
                $first_day_in_month=7;
            }
            echo '<table cellspacing="0">';
            echo '<th colspan="7"><div class="name-month" data-font-size="8">'.$months[$month-1].'</div></th>';
            echo '<tr><div class="days"><td>Lun</td><td>Mar</td><td>Mer</td><td>Jeu</td><td>Ven</td>';
            echo '<td class="sat">Sam</td><td class="sun">Dim</td></tr></div>';
            echo '<tr>';

            for($i=1; $i<$first_day_in_month; $i++) {
                echo '<td> </td>';
            }

            for($day=1; $day<=$month_days; $day++) {
                $pos=($day+$first_day_in_month-1)%7;
                $class = (($day==$current_day) && ($month==$current_month)) ? 'today' : 'day';
                $class .= ($pos==6) ? ' sat' : '';
                $class .= ($pos==0) ? ' sun' : '';

                echo '<td class="'.$class.'" data-font-size="6">'.$day.'</td>';
                if ($pos==0) echo '</tr><tr>';
            }
            echo '</tr>';
            echo '</table>';

            echo '</td>';
        }
        echo '</tr>';

    }
        echo '
            <tr class="footer" data-font-size="8">
                <td class="artiwiz">ARTIWIZ</td>
                <td>2, PLACE CLÉMENT - STRASBOURG</td>
                <td>TÉL +03 88 12 34 56 78</td>
                <td class="artiwiz"><a href="https://www.artiwiz.com">WWW.ARTIWIZ.COM</a></td>
            </tr>
        ';
    echo '</table>';
echo '</div>';

echo '<script src="js/jquery-3.2.1.min.js"></script>';
echo '<script src="js/main.js"></script>';
?>